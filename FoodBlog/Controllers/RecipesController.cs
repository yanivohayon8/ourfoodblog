﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodBlog.DAL;
using FoodBlog.Models;

namespace FoodBlog.Controllers
{
    public class RecipesController : Controller
    {
        private ProjectContext db = new ProjectContext();

        // GET: Recipes
        public ActionResult Index()
        {
            return View(db.Recipes.ToList());
        }

        // GET: Recipes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return HttpNotFound();
            }

            // Incerment the counter for this cheff
            int cheffId = recipe.Cheff.ID;
            string currCheffName = db.Cheffs.First(c => c.ID == cheffId).Name;
            Dictionary<string, int> counterDictionary = (Dictionary<string, int>)Session["counterDictionary"];
            if (counterDictionary is null)
            {
                counterDictionary = new Dictionary<string, int>();
            }
            if (counterDictionary.ContainsKey(currCheffName))
            {
                counterDictionary[currCheffName]++;
            }
            else
            {
                counterDictionary.Add(currCheffName, 1);
            }
            Session["counterDictionary"] = counterDictionary;

            return View(recipe);
        }

        // GET: Recipes/Create
        public ActionResult Create()
        {
            var allCheffs = db.Cheffs;
            this.ViewData["cheffsSelectable"] = (IEnumerable<FoodBlog.Models.Cheff>)allCheffs;
            return View();
        }

        // POST: Recipes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RecipeID,Name,CheffID,PublishDate, CreationProcess")] Recipe recipe)
        {
            recipe.PublishDate = DateTime.Now;
            if (ModelState.IsValid)
            {
                db.Recipes.Add(recipe);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            var allCheffs = db.Cheffs;
            this.ViewData["cheffsSelectable"] = (IEnumerable<FoodBlog.Models.Cheff>)allCheffs;
            return View(recipe);
        }

        // GET: Recipes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return HttpNotFound();
            }

            var allCheffs = db.Cheffs;
            this.ViewData["cheffsSelectable"] = (IEnumerable<FoodBlog.Models.Cheff>)allCheffs;
            return View(recipe);
        }

        // POST: Recipes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "Admin")]
        public ActionResult Edit([Bind(Include = "RecipeID,Name,CheffID,PublishDate,CheffNickName,CreationProcess")] Recipe recipe)
        {
            if (ModelState.IsValid)
            {
                db.Entry(recipe).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            var allCheffs = db.Cheffs;
            this.ViewData["cheffsSelectable"] = (IEnumerable<FoodBlog.Models.Cheff>)allCheffs;
            return View(recipe);
        }

        // GET: Recipes/Delete/5
        [Authorize(Users = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Recipe recipe = db.Recipes.Find(id);
            if (recipe == null)
            {
                return HttpNotFound();
            }
            return View(recipe);
        }

        // POST: Recipes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Recipe recipe = db.Recipes.Find(id);
            db.Recipes.Remove(recipe);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        // GET: Recipes
        public ActionResult Filter(string placeOfBirthInput, string CheffName, string CheffNickName, string RecipeName)
        {
            var results = db.Recipes.AsQueryable();
            Country placeOfBirth;
            if (!string.IsNullOrEmpty(CheffNickName))
            {
                results = from recipe in db.Recipes
                          join cheff in db.Cheffs on recipe.RecipeID equals cheff.ID
                          where cheff.NickName == CheffNickName
                          select recipe;
            }
            if (!string.IsNullOrEmpty(placeOfBirthInput) && Enum.TryParse(placeOfBirthInput, out placeOfBirth))
            {
                results = results.Where(c => c.Cheff.PlaceOfBirth == placeOfBirth);
            }
            if (!string.IsNullOrEmpty(CheffName))
            {
                results = results.Where(c => c.Cheff.Name == CheffName);
            }

            if (!string.IsNullOrEmpty(RecipeName))
            {
                results = results.Where(c => c.Name == RecipeName);
            }
            
            return View("Index", results.ToList());
        }

        public ActionResult GroupByCheff()
        {
            // Group by and join
            var totalRecipes = from recipe in db.Recipes
                             group recipe by recipe.CheffID into g
                             join cheff in db.Cheffs on g.Key equals cheff.ID
                             select new GroupByCheffModel() { CheffName = cheff.Name, TotalRecipes = g.Sum(p => 1) };

            return View(totalRecipes.ToList());
        }

        [HttpGet]
        public ActionResult GroupByCheffData()
        {
            // Group by and join
            var totalRecipes = from recipe in db.Recipes
                             group recipe by recipe.CheffID into g
                             join cheff in db.Cheffs on g.Key equals cheff.ID
                             select new GroupByCheffModel() { CheffName = cheff.Name, TotalRecipes = g.Sum(p => 1) };

            return Json(totalRecipes.ToList(), JsonRequestBehavior.AllowGet);
        }

        // Get Recipes/WantMore
        [HttpPost]
        public ActionResult WantMore(string cheffId, string currentRecipeId)
        {
            try
            {
                int recipeId = Int32.Parse(currentRecipeId);
                int chId = Int32.Parse(cheffId);
                var alikeRecipe = (from recipe in db.Recipes
                                 where recipe.RecipeID != recipeId && recipe.CheffID == chId
                                 select recipe.RecipeID).SingleOrDefault();
                if (alikeRecipe != 0)
                {
                    return Json(new Dictionary<string, object> { { "url", "/Recipes/Details/" + alikeRecipe } });
                }
                else
                {
                    return Json(new Dictionary<string, object> { { "error", "didnt found any" } });
                }
            }
            catch (Exception ex)
            {
                return Json(new Dictionary<string, object> { { "error", ex.Message } });
            }


        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
