﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FoodBlog.DAL;
using FoodBlog.Models;

namespace FoodBlog.Controllers
{
    public class CheffsController : Controller
    {
        private ProjectContext db = new ProjectContext();

        // GET: Cheffs
        public ActionResult Index(string CheffName, string CheffNickName, string placeOfBirthInput)
        {
            var results = db.Cheffs.AsQueryable();
            
            Country placeOFBirth;//

            if (!string.IsNullOrEmpty(CheffName))
            {
                // query join between cheffs and recipes
                results = from cheff in db.Cheffs
                          where cheff.Name == CheffName
                          select cheff;//,Name,CheffID,Name,NickName;
            }


            // recipe query by either last name or either first name
            if (!string.IsNullOrEmpty(CheffNickName))
            {
                // query join between cheffs and recipes
                results = from cheff in db.Cheffs
                          where cheff.NickName == CheffNickName 
                          select cheff;//,Name,CheffID,Name,NickName;
            }


            if (!string.IsNullOrEmpty(placeOfBirthInput) && Enum.TryParse(placeOfBirthInput, out placeOFBirth))
            {
                results = results.Where(s => s.PlaceOfBirth == placeOFBirth);
            }
            

            return View(results.ToList());
        }

        // GET: Cheffs
        [HttpGet]
        public ActionResult IndexData(string CheffName, string CheffNickName, string placeOfBirthInput)
        {
            var chefss = db.Cheffs.ToList();

            return Json(chefss, JsonRequestBehavior.AllowGet);
        }

        // GET: Cheffs/Details/5
        [Authorize(Users = "Admin")]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheff cheff = db.Cheffs.Find(id);
            if (cheff == null)
            {
                return HttpNotFound();
            }
            return View(cheff);
        }

        // GET: Cheffs/Create
        [Authorize(Users = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Cheffs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "Admin")]
        public ActionResult Create([Bind(Include = "ID,Name,NickName,BirthDay,PlaceOfBirth")] Cheff cheff)
        {
            if (ModelState.IsValid)
            {
                db.Cheffs.Add(cheff);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(cheff);
        }

        // GET: Cheffs/Edit/5
        [Authorize(Users = "Admin")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheff cheff = db.Cheffs.Find(id);
            if (cheff == null)
            {
                return HttpNotFound();
            }
            return View(cheff);
        }

        // POST: Cheffs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "Admin")]
        public ActionResult Edit([Bind(Include = "ID,Name,NickName,BirthDay,PlaceOfBirth")] Cheff cheff)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cheff).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cheff);
        }

        // GET: Cheffs/Delete/5
        [Authorize(Users = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Cheff cheff = db.Cheffs.Find(id);
            if (cheff == null)
            {
                return HttpNotFound();
            }
            return View(cheff);
        }

        // POST: Cheffs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Users = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
            Cheff cheff = db.Cheffs.Find(id);
            db.Cheffs.Remove(cheff);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
