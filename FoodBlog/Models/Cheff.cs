﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FoodBlog.Models
{

    public enum Country { France, GreatBritain, USA, Israel, India };

    public class Cheff
    {
        

        [Key]
        public int ID { get; set; }

        public string Name { get; set; }

        public string NickName { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime BirthDay { get; set; }

        public Country PlaceOfBirth { get; set; }
    }
}