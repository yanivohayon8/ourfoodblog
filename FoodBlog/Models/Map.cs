﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodBlog.Models
{
    public class Map
    {
        public int MapID { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }
    }
}