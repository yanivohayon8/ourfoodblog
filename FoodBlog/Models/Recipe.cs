﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FoodBlog.Models
{
    public class Recipe
    {
        [Key]
        public int RecipeID { get; set; }

        public string Name { get; set; }

        public int CheffID { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime PublishDate { get; set; }

        public string CreationProcess { get; set; }

        public virtual Cheff Cheff { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }

    public class GroupByCheffModel
    {
        public int CheffID { get; set; }
        public string CheffName { get; set; }
        public int TotalRecipes { get; set; }
    }
}